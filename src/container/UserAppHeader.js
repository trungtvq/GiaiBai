import React from 'react';
import { Navbar, NavDropdown, Nav, Form, FormControl, Button, InputGroup } from 'react-bootstrap';
import UserDropdownBtn from '../component/userDropdownBtn'
class GuestAppHeader extends React.Component {
  
  state = {
    selectedOption: null,
  };
  handleChange = selectedOption => {
    this.setState({ selectedOption });
    console.log(`Option selected:`, selectedOption);
  };
  render() {

    return (
      <Navbar sticky="top" collapseOnSelect expand="lg" bg="dark" variant="dark">
        <Navbar.Brand href="/">Giải Bài</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />

        <Navbar.Collapse id="responsive-navbar-nav">


          <Form inline>
            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
            <InputGroup>
            <InputGroup.Prepend>
                <InputGroup.Text id="btnGroupAddon">Lĩnh Vực</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl as="select">
                <option>Giáo Dục</option>
                <option>Kỹ Thuật</option>
                <option>Kinh Tế</option>
                <option>Thiết bị</option>

              </FormControl>
              <InputGroup.Prepend>
                <InputGroup.Text id="btnGroupAddon">Chủ đề</InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl as="select">
                <option>Toán</option>
                <option>Vật Lý</option>
                <option>Hóa</option>
              </FormControl>
             
            </InputGroup>
            <Button variant="outline-success">Tìm kiếm</Button>

          </Form>




        </Navbar.Collapse>
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="/">Tính năng</Nav.Link>
            <Nav.Link href="#pricing">Giá</Nav.Link>

            <NavDropdown title="Dropdown" id="collasible-nav-dropdown">
              <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
            </NavDropdown>
          </Nav>


          <Nav>
            <UserDropdownBtn />
          </Nav>

        </Navbar.Collapse>

      </Navbar>

    )
  }
}
export default GuestAppHeader;