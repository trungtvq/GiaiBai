import React from 'react';
import { Navbar, NavDropdown, Nav, Form,  Button,  Modal } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSignInAlt, faUserPlus } from '@fortawesome/free-solid-svg-icons'
import { GetTheme } from '../assets/theme'
import FacebookLogin from 'react-facebook-login';
import { GoogleLogin } from 'react-google-login';

class GuestAppHeader extends React.Component {

  state = {
    selectedOption: null,
    showLogin: false,
    showSignUp: false,

  };
 
  setShowLogin = showLogin => {
    this.setState({ showLogin });
  }
  setShowSignUp = showSignUp => {
    this.setState({ showSignUp });
  }
  handleChange = selectedOption => {
    this.setState({ selectedOption });
    console.log(`Option selected:`, selectedOption);
  };
  render() {
    const responseFacebook = (response) => {
      console.log(response);
    }
    const responseGoogle = (response) => {
      console.log(response);
    }
    return (
      <Navbar sticky="top" collapseOnSelect expand="lg" bg="dark" variant="dark">
        <Navbar.Brand href="/">Làm Thuê</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />

        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="/">Tính năng</Nav.Link>
            <Nav.Link href="#pricing">Giá</Nav.Link>

            <NavDropdown title="Dropdown" id="collasible-nav-dropdown">
              <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
            </NavDropdown>
          </Nav>


          <Nav>
            <>

              <Button variant="link" style={{ color: GetTheme().c2 }} onClick={() => this.setShowLogin(true)}>
                <FontAwesomeIcon icon={faSignInAlt} /> Đăng nhập
</Button>

              <Modal
                show={this.state.showLogin}
                onHide={() => this.setShowLogin(false)}
                dialogClassName="modal-90w"
                aria-labelledby="example-custom-modal-styling-title"
              >
                <Modal.Header closeButton>
                  <Modal.Title id="example-custom-modal-styling-title">
                    Đăng nhập
</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                <Form>
  <Form.Group controlId="formBasicEmail">
    <Form.Label>Email address</Form.Label>
    <Form.Control type="email" placeholder="Enter email" />
    <Form.Text className="text-muted">
      We'll never share your email with anyone else.
    </Form.Text>
  </Form.Group>

  <Form.Group controlId="formBasicPassword">
    <Form.Label>Password</Form.Label>
    <Form.Control type="password" placeholder="Password" />
  </Form.Group>
  <Form.Group controlId="formBasicCheckbox">
    <Form.Check type="checkbox" label="Check me out" />
  </Form.Group>
  <Button variant="primary" type="submit">
    Submit
  </Button>
  <FacebookLogin
    appId="287813625551814"
    autoLoad={false}
    fields="name,email,picture"
    callback={responseFacebook} />
  <GoogleLogin
    clientId="595655941041-29v7d0ta38dfkbbuciftjipido1rt5at.apps.googleusercontent.com"
    buttonText="Login with gmail"
    onSuccess={responseGoogle}
    onFailure={responseGoogle}
    cookiePolicy={'single_host_origin'}
  />,
  
</Form>
                </Modal.Body>
              </Modal>
            </>
            <>
              <Button variant="link" style={{ color: GetTheme().c1 }} onClick={() => this.setShowSignUp(true)}>
                <FontAwesomeIcon icon={faUserPlus} /> Đăng kí
        </Button>
              <Modal
                show={this.state.showSignUp}
                onHide={() => this.setShowSignUp(false)}
                dialogClassName="modal-90w"
                aria-labelledby="example-custom-modal-styling-title"
              >
                <Modal.Header closeButton>
                  <Modal.Title id="example-custom-modal-styling-title">
                    Đăng kí
            </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  ahihi
                </Modal.Body>
              </Modal>
            </>

          </Nav>

        </Navbar.Collapse>

      </Navbar>

    )
  }
}
export default GuestAppHeader;