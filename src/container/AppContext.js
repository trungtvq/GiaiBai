import React,{} from 'react'
import GuestAppHeader from './GuestAppHeader'
import UserAppHeader from './UserAppHeader'

import AppFooter from './AppFooter';
import AppBody from './AppBody';
import authContext from '../context/auth'
class AppContext extends React.Component {
    constructor(props) {
        super(props);

        // State also contains the updater function so it will
        // be passed down into the context provider
        this.state = {
            token: null,
            signInEmail: null,
        };
        
        this.login = (token, signInEmail) => {
            this.setState({ token: token, signInEmail: signInEmail });
        }
        this.logout = () => {
            this.setState({ token: null, signInEmail: null });
        }
    }
    render() {
        return (
            <authContext.Provider value={{
                token:this.state.token,
                signInEmail: this.state.token,
                login:this.login,
                logout: this.logout,
            }}>
                {this.state.token ==null? <GuestAppHeader /> :<UserAppHeader />}
                
                <AppBody />
                <AppFooter />
            </authContext.Provider>
        )
    }
}
export default AppContext;