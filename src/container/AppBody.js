import React from 'react';
import AppRouter from './AppRouter'
class AppBody extends React.Component {

    render() {
        return (
           <div>              
               <AppRouter />
           </div>
        )
    }
}
export default AppBody;